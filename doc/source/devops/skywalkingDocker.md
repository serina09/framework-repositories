# Skywalking Docker

SkyWalking: an APM(application performance monitor) system, especially designed for microservices, cloud native and container-based (Docker, Kubernetes, Mesos) architectures.

## Docker Compose
`skywalking.yml`

[http://localhost:8080/](http://localhost:8080/)

## Screenshots
![](https://skywalking.apache.org/ui-doc/8.9.0/dashboard.png)

![](https://skywalking.apache.org/ui-doc/8.4.0/topology.png)

![](https://skywalking.apache.org/ui-doc/7.0.0/trace.png)

## References
- [skywalking-oap-server Docker](https://hub.docker.com/r/apache/skywalking-oap-server)
- [skywalking-ui Docker](https://hub.docker.com/r/apache/skywalking-ui)